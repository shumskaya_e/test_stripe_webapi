﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Stripe;

namespace StripeWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChargesController : ControllerBase
    {
        private string _stripe_pk_key;

        public ChargesController(IConfiguration configuration)
        {
            _stripe_pk_key = configuration.GetSection("Stripe_Key").Value;
        }

        public class CreateRequest
        {
            public string SourceId { get; set; }

            public string CustomerId { get; set; }

            public string ReceiptEmail { get; set; }

            public string OnBehalfOf { get; set; }

            public string TransferGroup { get; set; }

            public decimal? ExchangeRate { get; set; }

            public string Description { get; set; }
  
            public bool? Capture { get; set; }

            public long? ApplicationFee { get; set; }

            public string Currency { get; set; }

            public long? Amount { get; set; }

            public string StatementDescriptor { get; set; }

            public long? ExpMonth { get; set; }

            public long? ExpYear { get; set; }

            public string Number { get; set; }

            public string AddressCity { get; set; }

            public string AddressCountry { get; set; }

            public string AddressLine1 { get; set; }

            public string AddressLine2 { get; set; }

            public string AddressState { get; set; }
 
            public string AddressZip { get; set; }
            
            public string Cvc { get; set; }

            public string Name { get; set; }

            public object MetaData { get; set; }

            public string IssuingCardId { get; set; }
        }
    

        // GET api/charges
        [HttpGet]
        public ActionResult<IEnumerable<Charge>> Get()
        {
            StripeConfiguration.SetApiKey(_stripe_pk_key);

            var service = new ChargeService();
            var options = new ChargeListOptions();
            StripeList<Charge> charges = service.List(options); 
            return charges.ToList();
        }

        // GET api/charges/5
        [HttpGet("{id}")]
        public ActionResult<Charge> Get(string id)
        {
            StripeConfiguration.SetApiKey(_stripe_pk_key);

            var service = new ChargeService();
            Charge charge = service.Get(id);
            return charge;
        }

        // POST api/charges
        [HttpPost]
        public ActionResult<Charge> Post([FromBody] CreateRequest requestOptions)
        {
            StripeConfiguration.SetApiKey(_stripe_pk_key);

            var options = new TokenCreateOptions
            {
                Card = new CreditCardOptions
                {
                    Number = requestOptions.Number,
                    ExpYear = requestOptions.ExpYear,
                    ExpMonth = requestOptions.ExpMonth,
                    Cvc = requestOptions.Cvc
                }
            };

            var service = new TokenService();
            Token stripeToken = service.Create(options);
            var chargeCreateOptions = new ChargeCreateOptions
            {
                Amount = requestOptions.Amount,
                Currency = requestOptions.Currency,
                Description = requestOptions.Description,
                SourceId = stripeToken.Id
            };
            var chargeService = new ChargeService();
            Charge charge = chargeService.Create(chargeCreateOptions);
            return charge;
        }

        // PUT api/charges/5
        [HttpPut("{id}")]
        public void Put(string id, [FromBody] ChargeUpdateOptions options)
        {
            StripeConfiguration.SetApiKey(_stripe_pk_key);

            var service = new ChargeService();
            Charge charge = service.Update(id, options);
        }


        // PUT api/charges/id/capture
        [HttpPut("{id}")]
        public void Put(string id, [FromBody] ChargeCaptureOptions options)
        {
            StripeConfiguration.SetApiKey(_stripe_pk_key);

            var service = new ChargeService();
            Charge charge = service.Capture(id, options);
        }
    }
}
