﻿const uri = "api/charges";
let todos = null;
function getCount(data) {
    const el = $("#counter");
    let name = "to-do";
    if (data) {
        if (data > 1) {
            name = "to-dos";
        }
        el.text(data + " " + name);
    } else {
        el.text("No " + name);
    }
}
$(document).ready(function () {
    getData();
});
function getData() {
    $.ajax({
        type: "GET",
        url: uri,
        cache: false,
        success: function (data) {
            const tBody = $("#todos");
            $(tBody).empty();
            getCount(data.length);
            $.each(data, function (key, item) {
                const tr = $("<tr></tr>")
                    .append($("<td></td>").text(item.id))
                    .append($("<td></td>").text(item.amount))
                    .append($("<td></td>").text(item.description));
                tr.appendTo(tBody);
            });
            todos = data;
        }
    });
}
function addItem() {
    const options = {
        Amount: $("#amount").val(),
        Currency: $("#currency").val(),
        Description: $("#description").val(),
        ExpYear: $("#year").val(),
        ExpMonth: $("#month").val(),
        Cvc: $("#cvc").val(),
        Number: $("#number").val()
    };
    $.ajax({
        type: "POST",
        accepts: "application/json",
        url: uri,
        contentType: "application/json",
        data: JSON.stringify(options),
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong!");
        },
        success: function (result) {
            //getData();
            var jsonStr = JSON.stringify(result);
            var node = new PrettyJSON.view.Node({
                el: $('#response'),
                data: result
            });
            //$('div #response').html(jsonStr);
        }
    });
}
function deleteItem(id) {
    $.ajax({
        url: uri + "/" + id,
        type: "DELETE",
        success: function (result) {
            getData();
        }
    });
}
function editItem(id) {
    $.each(todos, function (key, item) {
        if (item.id === id) {
            $("#edit-name").val(item.name);
            $("#edit-id").val(item.id);
            $("#edit-isComplete")[0].checked = item.isComplete;
        }
    });
    $("#spoiler").css({ display: "block" });
}
function closeInput() {
    $("#spoiler").css({ display: "none" });
}